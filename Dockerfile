FROM node
RUN apt-get update && apt-get install -y nano
RUN mkdir /spellpir
COPY . /spellpir
WORKDIR /spellpir

RUN yarn install
RUN yarn test
RUN yarn build

#yarn start
CMD yarn start

EXPOSE 3000
123